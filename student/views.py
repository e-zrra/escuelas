from django.shortcuts import render, get_object_or_404, redirect
from .models import Student
from .forms import StudentForm
import json
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def index (request):
	students = Student.objects.order_by('-id')
	page = request.GET.get('page')
	paginator = Paginator(students, 10)
	try:
		students = paginator.page(page)
	except PageNotAnInteger:
		students = paginator.page(1)
	except EmptyPage:
		students = paginator.page(paginator.num_pages)

	#if request.is_ajax():
	#	return render(request, 'students/students.html', { 'students' : students })

	return render(request, 'students/index.html', { 'students' : students })

def new (request):
	if request.method == "POST":
		try:
			form = StudentForm(request.POST)
			if form.is_valid():
				student = form.save(commit=False)
				student.save()
				data = {}
				data['msg'] = 'student created'
				data['success'] = True
				return HttpResponse(json.dumps(data), content_type="application/json")
			else:
				return HttpResponse(json.dumps(data), content_type="application/json")
				#return errors
		except Exception, e:
			return e
			#return exception 
	else:
		form = StudentForm()
	return render(request, 'students/new.html', { 'form' : form })

def show (request, pk):
	return render(request, 'students/show.html')

def edit (request, pk):
	student = get_object_or_404(Student, pk=pk)
	if request.method == 'POST':
		form = StudentForm(request.POST, instance=student)
		data = {}
		if form.is_valid():
			student = form.save(commit=False)
			student.save()
			data['msg'] = 'student updated'
			data['success'] = True
		else:
			data['msg'] = 'student error'
			data['success'] = False
		return HttpResponse(json.dumps(data), content_type="application/json")
	else:
		form = StudentForm(instance=student)
		return render(request, 'students/edit.html', {'form' : form, 'student' : student })