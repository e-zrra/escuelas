from django.db import models

class Student(models.Model):
    name          	= models.CharField(max_length=50)
    last_name       = models.CharField(max_length=140)
    # grupo           = models.ForeignKey(Grupo)
    status       	= models.BooleanField(default=True)
    register_date  	= models.DateTimeField(auto_now_add=True)

    # def save (self):
    # 	self.slug = slugify(self.slug)
    # 	super(test, self).save()

    def __str__ (self):
    	return self.name
