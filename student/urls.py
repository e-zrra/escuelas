from django.conf.urls import patterns, include, url
from . import views

urlpatterns = patterns('',
	url(r'^$', views.index, name="index"),
	url(r'^(?P<pk>[0-9]+)/$', views.show, name="show"),
	url(r'^new/$', views.new, name='new'),
	url(r'^(?P<pk>[0-9]+)/edit/$', views.edit, name="edit"),
)