from django.shortcuts import render

# @login_required()
def index(request):
    return render(request,'institution/index.html', locals())

def groups(request):
	return render(request, 'institution/groups/index.html');

def grades(request):
	return render(request, 'institution/grades/index.html')
