from django.conf.urls import patterns, include, url
from . import views

urlpatterns = patterns('',
    url(r'^$', views.index),
    
    #groups
    url(r'^groups/$', views.groups, name="groups"),
    #url(r'^groups/new$', views.groups_new, name="groups_new"),

    #grades
    url(r'^grades/$', views.grades, name="grades"),
    #url(r'^grades/new$', views.grades_new, name="grades_new"),
)
