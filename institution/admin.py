from django.contrib import admin
from .models import Institution, Grade, Group

admin.site.register(Institution)
admin.site.register(Grade)
admin.site.register(Group)