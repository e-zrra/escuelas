from django.db import models

# class TipoOrganizacion(models.Model):
#     descripcion = models.CharField(max_length=140)
#     es_activo = models.BooleanField(default=True)
#     fecha_registro = models.DateTimeField(auto_now_add=True)

#     def __str__(self):
#         return self.descripcion

class Institution(models.Model):
    name                = models.CharField(max_length=140)
    slug                = models.SlugField()
    description         = models.TextField()
    #logo               = models.ImageField()
    status              = models.BooleanField(default=True)
    registration_date   = models.DateTimeField(auto_now_add=True)
    address             = models.TextField()
    phone_number        = models.CharField(max_length=30)
    # tipo_organizacion = models.ForeignKey(TipoOrganizacion)

    def save(self):
        self.slug = slugify(self.slug)
        super(test, self).save()

    def __str__(self):
        return self.nombre

class Grade(models.Model):
    name        = models.CharField(max_length=140)
    description = models.TextField()
    status      = models.BooleanField(default=True)

# class Clase(models.Model):
#     nombre = models.CharField(max_length=140)
#     descripcion = models.TextField()
#     es_activo = models.BooleanField(default=True)
#     fecha_registro = models.DateTimeField(auto_now_add=True)

class Group(models.Model):
    name        = models.CharField(max_length=140)
    description = models.TextField()
    # grado = models.ForeignKey(Grado)
    # clases = models.ManyToManyField(Clase, through='ClaseGrupo')
    # es_activo = models.BooleanField(default=True)
    # fecha_registro = models.DateTimeField(auto_now_add=True)

# class ClaseGrupo(models.Model):
#     grupo = models.ForeignKey(Grupo)
#     clase = models.ForeignKey(Clase)
#     fecha_registro = models.DateTimeField(auto_now_add=True)