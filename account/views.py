from django.shortcuts import render,redirect,render_to_response,get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.template import RequestContext
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from .forms import UserForm
from django.contrib.auth.models import User


# @login_required()
def index(request):
     return render(request,'index/index.html',locals())


#Auth views
def log_in(request):
    context = RequestContext(request)

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request,user)
                return redirect(reverse('account.views.index'))
            else:
                return HttpResponse("Cuenta desactivada.")
        else:
            message = "Datos incorrectos"
            return render(request,'account/login.html', locals())

    else:
        return render_to_response('account/login.html', {}, context)

@login_required
def log_out(request):
    logout(request)
    return redirect(reverse(login))

@login_required
def profile(request):
    post = get_object_or_404(User, pk=request.user.pk)
    if request.method == "POST":
        form = UserForm(request.POST, instance=post)
        if form.is_valid():
            user = form.save(commit = False)
            user.set_password(user.password)
            user.save()
            return redirect(reverse('account.views.profile'))
    else:
        form = UserForm(instance=post)
    return render(request, 'account/profile.html', locals())
