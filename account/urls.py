from django.conf.urls import patterns, include, url
from . import views

urlpatterns = patterns('',
    url(r'^$', views.index),
    url(r'^login/$', views.log_in),
    url(r'^logout/$', views.log_out),
    url(r'^profile/$', views.profile),

)
