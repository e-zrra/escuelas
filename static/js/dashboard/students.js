$(document).ready(function () {

	$('.btn-edit-student').on('click', function () {

		var tr = $('#content-table-student').find('.highlighted');

		if (tr.hasClass('highlighted')) {
			
			var id = tr.attr('id');

			if (id) {


				edit_student_modal(id);
			};

		} else {

			alert('Selecciona un registro de la tabla')

		}

	});

	function edit_student_modal (id) {

		$.get("/students/" + id + '/edit', function( data ) {

	  		$('#modal-student').modal('show');

	  		$('.modal-content-student-form').html(data);

	  		form_student();

		});
	}

	/*$(window).on('hashchange', function () {

		if (window.location.hash) {

			var page = window.location.hash.replace('#', '');

			if (page == Number.NaN || page <= 0 ) {

				return false;

			} else {

				get_students(page);
			}
		};
	});*/

	/*$(document).on('click', '.pagination a', function (e) {

		var page = $(this).attr('href').split('page=')[1];
		
		get_students(page);

		e.preventDefault();
	});*/

	/*function get_students (page) {

		$.ajax({
			url : '?page=' + page,
			dataType: 'html',
			type: 'GET',
			beforeSend: function () {

			},
			success: function (data) {
				$('#content-table-student').html(data);
				//location.hash = page;
			},
			error: function (data) {
				alert('Student could not be loaded')
			},
			complete: function () {

			}
		});
	}*/

	$('.btn-modal-student-form').on('click', function () {

		$.get("/students/new", function( data ) {

	  		$('#modal-student').modal('show');

	  		$('.modal-content-student-form').html(data);

	  		form_student();

		});
	});

	function form_student () {

		$('#form-student').validate({
			rules: {
			    name: {
			      	required: true
			    },
			    last_name: {
			      	required: true
			    }
			},
			submitHandler: function(form) {
			    var button, text;
			    button = $('#btn-modal-save');
			    text = button.val();

			    $(form).ajaxSubmit({
			      	beforeSend: function() {
			        	return button.val('Saving...').prop('disabled', true);
			      	},
			      	error: function() {
			        	alertBox('danger', 'Server Response', 'Error', 'Ok');
			      	},
			      	success: function(response) {

				        if (response.success) {

				        	//form.reset()

				        	console.log(response)

				        	/*if (typeof response.data != 'undefined') {

				        		$('.row-' + response.data.id).remove();

				        		var row =  '<tr class="row-' + response.data.id + ' btn-modal-line-edit"><input type="hidden" value="'+response.data.id +'"><td><input type="checkbox" name="action"> </td>';
							    row 	+= '<td>' + response.data.name +'</td>';
							    row 	+= '<td>' + response.data.description + '</td>';
							    row 	+= '<td>' + response.data.created_at +'</td>';
							    row 	+= '</tr>';

							    $('.table-line tbody').prepend(row);

				        	};*/

				            if (response.msg) {

				            	alertBox('success', 'Information', response.msg, 'Ok');
				            };

				            if (response.redirect) {
				            	window.location.href = response.redirect;
				            }

				            $('.alert-data-not-found').remove();

				        } else {

				        	alertBox('success', 'Information', response.msg, 'Ok');
				        }
			      	},
			      	complete: function() {
			        	return button.val(text).prop('disabled', false);
			      	},

			      	dataType: 'JSON'
			    });
			}
		});

	}

});