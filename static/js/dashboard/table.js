$(document).ready(function () {

	$('table tbody tr').on('click', function () {

		$(this).closest('tr').siblings().removeClass('highlighted');

		$(this).toggleClass('highlighted');

	});

})